import gitlab
import json
import csv
import re
import base64
import yaml
import collections
import argparse
import markdown
from dateutil.parser import *
from dateutil.relativedelta import *

# https://stackoverflow.com/questions/7204805/dictionaries-of-dictionaries-merge/7205107#7205107
def merge_dicts(a, b, path=None):
    "merges b into a"
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                 merge_dicts(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass # same leaf value
            elif isinstance(a[key], list) and isinstance(b[key], list):
                a[key].extend(b[key])
            else:
                raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a

def merge_post(project, path):
    tree = project.repository_tree(path, as_list=False)
    features = {}
    i = 0
    for blob in tree:
        file_info = p.repository_blob(blob['id'])
        print("Getting file "+blob["name"])
        content = base64.b64decode(file_info['content'])
        #print(yaml.safe_load(content.decode("utf-8")))
        try:
            features = merge_dicts(features, yaml.safe_load(content.decode("utf-8")))
        except:
            print("Can not merge data from file: %s" % blob["name"])
    return features

def normalizeVersionString(version):
    version = version.split("_")
    major = version[0]
    minor = version[1]
    if len(major) < 2:
        major = "0" + major
    if len(minor) < 2:
        minor = "0" + minor
    return "%s_%s" % (major, minor)

def clean_description(desc_string):
    desc_string = re.sub("<img.*>","", desc_string)
    #desc_string = desc_string.replace("\n"," ")
    md = markdown.Markdown()
    desc_string = md.convert(desc_string)
    return desc_string

def reformat_features(op):
    feature_data = []
    for release in op:
        categories = op[release]
        for category in categories["features"]:
            for feature in categories["features"][category]:
                feature["rank"] = category
                feature["version"] = release
                feature["description"] = clean_description(feature["description"])
                feature_data.append(feature)
    return feature_data

def reformat_deprecations(op):
    feature_data = []
    for release in op:
        categories = op[release]
        if "deprecations" in categories:
            for feature in categories["deprecations"]:
                feature["version"] = release
                feature["due"] = clean_date(feature["due"])
                feature["description"] = clean_description(feature["description"])
                feature_data.append(feature)
    return feature_data

def clean_date(datestring):
    try:
        return str(parse(datestring))
    except:
        #people are referencing a date using a gitlab release version string
        return get_release_date(datestring)

def get_release_date(versionString):
    print("Computing date from version string %s " % versionString)
    major_dates = {"11":"2018-06-22","12":"2019-06-22","13":"2020-05-22","14":"2021-05-22"}
    #cut off the gitlab part first
    if "." not in versionString:
        return None
    versionString = versionString.split(" ")[1]
    major = versionString.split(".")[0]
    minor = versionString.split(".")[1]
    if major in major_dates:
        majordate = major_dates[major]
        date = parse(majordate) + relativedelta(months=+int(minor))
        return str(date)
    else:
        #fall back to computing from latest release. This will get inaccurate at some point
        majordate = major_dates["14"]
        major_delta = int(major) - 14
        month_delta = major_delta * 12 + int(minor)
        date = parse(majordate) + relativedelta(months=+month_delta)
        return str(date)

def make_feature_row(version, rank, feature):
    row = []
    row.append(version)
    row.append(rank)
    row.append(feature["name"])
    row.append(",".join(feature["available_in"]))
    #markdown to html
    row.append(clean_description(feature["description"]))
    
    links = ""
    if "documentation_link" in feature:
        if feature["documentation_link"] is not None:
            links += "<a href=" + feature["documentation_link"] + ">Documentation</a><br>"
    if "issue_url" in feature:
        links += "<a href=" + feature["issue_url"] + ">Issue</a><br>"
    if "video" in feature:
        links += "<a href=" + feature["video"] + ">Video</a><br>"
    row.append(links)
    
    if "stage" in feature:
        row.append(feature["stage"])
    else:
        row.append("")
    
    if "categories" in feature:
        row.append(",".join(feature["categories"]))
    else:
        row.append("")
    return row

parser = argparse.ArgumentParser(description='Crawl and report on GitLab features')

gl = gitlab.Gitlab("https://gitlab.com")
p = gl.projects.get("gitlab-com%2Fwww-gitlab-com")
release_posts = p.repository_tree("data/release_posts", as_list=False)
all_posts = {}
for release_post in release_posts:
    if "released.yml" in release_post["name"]:
        #treat as release post
        file_info = p.repository_blob(release_post['id'])
        print("Getting file " + release_post["name"])
        content = base64.b64decode(file_info['content'])
        version = re.findall(r'gitlab_[0-9]+_[0-9]+', release_post['name'])
        if version:
            version = normalizeVersionString(version[0].replace("gitlab_",""))
            all_posts[version] = yaml.safe_load(content.decode("utf-8"))
        else:
            print("No version string in %s, skipping." % release_post["name"])
    elif release_post["type"] == "tree" \
      and re.search("[0-9]+_[0-9]+", release_post["name"]) :
        version = normalizeVersionString(release_post["name"])
        all_posts[version] =  merge_post(p, "data/release_posts/"+release_post["name"])

op = collections.OrderedDict(sorted(all_posts.items()))

header = ["version","rank","name","available_in","description","links","stage","categories"]

with open('features.json', 'w') as outfile:
    json.dump(reformat_features(op), outfile, indent=4)

with open('deprecations.json', 'w') as outfile:
    json.dump(reformat_deprecations(op), outfile, indent=4)

rows = []
version_order = set()
for version in op:
    version_order.add(version)
    post = op[version]
    # as of 12.9, top is not used consistently any more
    if "top" in post["features"]:
        for feature in post["features"]["top"]:
            rows.append(make_feature_row(version, "top", feature))
    for feature in post["features"]["primary"]:
        rows.append(make_feature_row(version, "primary", feature))
    for feature in post["features"]["secondary"]:
        rows.append(make_feature_row(version, "secondary", feature))

with open("features.csv","w") as featurefile:
    featurewriter = csv.writer(featurefile, delimiter='\t',quoting=csv.QUOTE_MINIMAL)
    featurewriter.writerow(header)
    for row in rows:
        featurewriter.writerow(row)
